<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('/users', 'UsersController');

Route::get('/petitions', 'PetitionsController@index');
Route::get('/petitions/{id}', 'PetitionsController@show');
Route::post('/petitions', 'PetitionsController@store');
Route::put('/petitions/{id}/{userId}', 'PetitionsController@update');
Route::delete('/petitions/{id}', 'PetitionsController@destroy');
Route::get('/driver_petitions/{userId}', 'PetitionsController@driverPetitions');

Route::apiResource('/departments', 'DepartmentsController');
Route::get('/departments/{departmentId}/user_types/{utId}', 'DepartmentsController@byType');
Route::get('/department_petitions/{id}', 'DepartmentsController@petitions');

Route::get('/petition_participants/{petitionId}', 'PetitionParticipantsController@index');
Route::post('/petition_participants', 'PetitionParticipantsController@store');
Route::put('/petition_participants/{id}', 'PetitionParticipantsController@update');
Route::delete('/petition_participants/{id}', 'PetitionParticipantsController@destroy');

Route::post('/login', 'UsersController@login');

Route::get('/petition_resources/{petitionId}', 'PetitionResourcesController@index');
Route::post('/petition_resources', 'PetitionResourcesController@store');
Route::put('/petition_resources/{petitionId}', 'PetitionResourcesController@update');
Route::delete('/petition_resources/{petitionId}', 'PetitionResourcesController@destroy');

// logs por peticion
Route::get('/driver_logs/{petitionId}', 'DriverLogsController@index');
// log especifico
Route::get('/driver_log/{id}', 'DriverLogsController@show');
Route::post('/driver_logs', 'DriverLogsController@store');

Route::get('/receipt/{petitionId}', 'ReceiptsController@index');
Route::get('/receipt_details/{id}', 'ReceiptsController@show');
Route::post('/receipt', 'ReceiptsController@store');
Route::put('/receipt/{id}', 'ReceiptsController@update');


/**************************** Autocompletado ****************************/
Route::get('/states', 'StatesController@index');

Route::get('/user_types', 'UserTypesController@index');
Route::get('users_by_type/{id}', 'UserTypesController@users');

Route::get('/activity_types', 'ActivityTypesController@index');

Route::get('/resource_types', 'ResourceTypesController@index');

Route::get('/petition_status', 'PetitionStatusController@index');
Route::get('/petition_status/{id}', 'PetitionStatusController@show');

/**************************** Reportes ****************************/
Route::get('/petitions_by_state/{from}/{to}', 'StatesController@byState');

Route::get('/most_petitions_by_user', 'UsersController@mostPetitions');
Route::get('/most_changes_by_user', 'UsersController@mostChanges');
Route::get('/most_petitions_by_department', 'DepartmentsController@mostPetitions');
Route::get('/most_changes_by_department', 'DepartmentsController@mostChanges');

Route::get('/petitions_by_year/{year}', 'PetitionsController@byYear');

Route::get('/average_approval_time', 'PetitionsController@averageApprovalTime');

Route::get('/average_cost', 'ReceiptsController@averageCost');