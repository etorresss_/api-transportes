<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetitionStatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public function petition() {
        return $this->hasMany('App\Petition');
    }
}
