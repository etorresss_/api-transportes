<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Notifier extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Cambio de estado de solicitud Nº ';

    public $petition, $department, $user, $title;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->petition = $mailData['petition'];
        $this->department = $mailData['department'];
        $this->user = $mailData['user'];

        $this->subject .= $this->petition->id;

        switch($this->petition->petition_status_id){
            case 4:
                $this->title = 'La solicitud Nº'.$this->petition->id.' fue RECHAZADA';
                break;
            case 5:
                $this->title = 'La solicitud Nº'.$this->petition->id.' fue APROBADA';
                break;
            default:
                $this->title = 'La solicitud Nº'.$this->petition->id.' requiere tu aprobación';
                break;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.notification');
    }
}
