<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetitionParticipant extends Model
{
    protected $fillable = [
        'petition_id', 'name', 'last_name', 'identification_number',
        'school_id', 'pickup_address', 'dropoff_address', 'pickup_time',
        'dropoff_time', 'in_charge'
    ];

    public function petition() {
        return $this->belongsTo('App\Petition');
    }

    public function driverLogs() {
        return $this->hasMany('App\DriverLog', 'id', 'checked_by');
    }
}
