<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverLog extends Model
{
    protected $fillable = [
        'user_id', 'petition_id', 'destination',
        'checked_by', 'approved', 'comment'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function petition() {
        return $this->belongsTo('App\Petition');
    }

    public function petitionParticipant() {
        return $this->belongsTo('App\PetitionParticipant', 'id', 'checked_by');
    }
}
