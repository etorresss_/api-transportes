<?php

namespace App\Http\Controllers;

use App\ResourceType;
use Illuminate\Http\Request;

class ResourceTypesController extends Controller
{
    public function index(){
        $restype = new ResourceType();

        return $restype->all();
    }
}
