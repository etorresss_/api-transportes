<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $user = new User();
        return $user::all();
    }

    public function login(Request $request)
    {
        $user = User::where('username', $request->username)->get()[0];

        return Hash::check($request->password, $user->password) ? $user : [];
    }

    public function store(Request $request)
    {
        $user = new User([
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'name' => $request->name,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'phone' => $request->phone,
            'user_type_id' => $request->user_type
        ]);

        $user->save();

        return $user;
    }

    public function show($user)
    {
        $user = User::findOrFail($user);

        return $user;
    }

    public function update(Request $request, $user)
    {
        $user = User::findOrFail($user);
        $user->fill($request->all());

        $user->save();

        return $user;
    }

    public function destroy($user)
    {
        return User::destroy($user);
    }

    public function mostPetitions()
    {
        $data = DB::table('users')
                    ->join('petitions', 'users.id', '=', 'petitions.user_id')
                    ->select(DB::raw('CONCAT(users.name, " ", users.lastname) as Nombre,
                    COUNT(petitions.user_id) as Cantidad'))
                    ->groupBy('users.name', 'users.lastname')
                    ->orderBy('Cantidad', 'desc')
                    ->get();

        return prepareReport($data);
    }

    public function mostChanges()
    {
        $data = DB::table('users')
                    ->join('change_logs', 'users.id', '=', 'change_logs.user_id')
                    ->select(DB::raw('CONCAT(users.name, " ", users.lastname) as Nombre,
                    COUNT(change_logs.user_id) as Cantidad'))
                    ->groupBy('users.name', 'users.lastname')
                    ->orderBy('Cantidad', 'desc')
                    ->get();

        return prepareReport($data);
    }
}
