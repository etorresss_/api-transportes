<?php

namespace App\Http\Controllers;

use App\UserType;
use Illuminate\Http\Request;

class UserTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $usertype = new UserType();

        return $usertype->all();
    }

    public function users($id)
    {
        return UserType::find($id)->users;
    }
}
