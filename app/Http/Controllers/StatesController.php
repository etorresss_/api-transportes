<?php

namespace App\Http\Controllers;

use App\State;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatesController extends Controller
{
    public function index(){
        $state = new State();

        return $state->all();
    }

    public function byState($from, $to) {
        $data = [$from, $to];
        array_push($data, '2050');
        $results = DB::select(DB::raw(
            'SELECT s.name as Provincia, count(p.state_id) AS Cantidad
            FROM states s
            INNER JOIN petitions p ON s.id = p.state_id
            WHERE YEAR(p.departure_time) >= COALESCE(?, "1990")
            AND YEAR(p.arrival_time) <= COALESCE(?, ?)
            GROUP BY p.state_id
            ORDER BY Cantidad DESC'
        ), $data);

        return prepareReport($results);
    }
}
