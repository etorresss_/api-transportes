<?php

namespace App\Http\Controllers;

use App\ChangeLog;
use App\Department;
use App\Mail\Notifier;
use App\Petition;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PetitionsController extends Controller
{
    public function index() {
        $petition = new Petition();

        return $petition::all();
    }

    public function store(Request $request) {
        $petition = Petition::create($request->all());
        $petition->needs_vice_rector = 
        needsViceRector($petition->departure_time, $petition->arrival_time);

        $petition->save();
        $user = User::where([
            ['department_id', $petition->department_id],
            ['user_type_id', 2]
        ])->get()[0];

        $mailData['petition'] = $petition;
        $mailData['department'] = Department::find($petition->department_id);
        $mailData['user'] = User::find($petition->user_id);
        Mail::to($user->email)->send(new Notifier($mailData));
        return $petition;
    }

    public function show($id) {
        return Petition::findOrFail($id);
    }

    public function update(Request $request, $id, $userId) {
        $petition = Petition::findOrFail($id);

        $cols = array_keys($request->all());

        if (sizeof($cols)) {
            $mailData = [];
            DB::transaction(function () use ($cols, $request, $id, $userId, $petition) {
                $petition->fill($request->all());
                needsViceRector($petition->departure_time, $petition->arrival_time);

                //si se cambia mas de una columna vuelve a estar pendiente para el jefe
                if (sizeof($cols) > 1 || !in_array("petition_status_id", $cols)) {
                    $petition->petition_status_id = 1;
                    foreach ($cols as $col) {
                        ChangeLog::create([
                            'petition_id' => $id,
                            'user_id' => $userId,
                            'column_name' => $col
                        ]);
                    }
                }

                $user = new User();
                switch ($petition->petition_status_id) {
                    case 1:
                        //notificar jefe
                        $user = User::where([
                            ['department_id', $petition->department_id],
                            ['user_type_id', 2]
                        ])->get()[0];
                        break;
                    case 2:
                        //notificar rectoria
                        $user = User::where([
                            ['department_id', 1],
                            ['user_type_id', 1]
                        ])->get()[0];
                        break;
                    case 3:
                        //notificar transportes
                        $user = User::where([
                            ['department_id', 2],
                            ['user_type_id', 4]
                        ])->get()[0];
                        break;
                    default:
                        //notificar usuario
                        $user = User::find($petition->user_id);
                        break;
                }
                $petition->save();
                $mailData['petition'] = $petition;
                $mailData['department'] = Department::find($petition->department_id);
                $mailData['user'] = User::find($petition->user_id);
                Mail::to($user->email)->send(new Notifier($mailData));
            });
        }

        return $petition;
    }

    public function destroy($id) {
        return Petition::destroy($id);
    }

    public function driverPetitions($userId) {
        return Petition::select('petitions.*')
                ->join('petition_resources', 'petitions.id', '=', 'petition_resources.petition_id')
                ->where([
                    ['petition_resources.resource_type_id', 1],
                    [DB::raw('CAST(petition_resources.value AS UNSIGNED)'), $userId]
                ])->get();
    }

    public function byYear($year) {;
        $result = DB::select(DB::raw('
            SELECT t.Mes, COUNT(t.Mes) as Cantidad
            FROM (SELECT ELT(DATE_FORMAT(p.departure_time,\'%m\'),
                    \'Enero\',\'Febrero\',\'Marzo\',\'Abril\',
                    \'Mayo\',\'Junio\', \'Julio\', \'Agosto\',
                    \'Setiembre\', \'Octubre\', \'Noviembre\',\'Diciembre\') as Mes
                FROM petitions p
                WHERE YEAR(p.departure_time) = ?) t
            GROUP BY t.Mes
            ORDER BY Cantidad DESC
        '), [$year]);

        return prepareReport($result);
    }

    public function averageApprovalTime() {
        $data = DB::table('petitions')
                    ->select(DB::raw('AVG(TIMEDIFF(updated_at, created_at)) as Promedio'))
                    ->where('petition_status_id', 5)->get();

        $val = $data[0]->Promedio;
        $data[0]->Promedio = intdiv($val, 240000).' d, '.
                            intdiv($val, 10000)%24 .' h, ';

        $val = $val%10000;
        $data[0]->Promedio .= intdiv($val, 100).' m, ';

        $val = $val%100;
        $data[0]->Promedio .= $val.'s';

        return prepareReport($data);
    }
}
