<?php

namespace App\Http\Controllers;

use App\PetitionStatus;
use Illuminate\Http\Request;

class PetitionStatusController extends Controller
{
    public function index() {
        return PetitionStatus::all();
    }

    public function show($id) {
        return PetitionStatus::find($id);
    }
}
