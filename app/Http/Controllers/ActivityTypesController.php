<?php

namespace App\Http\Controllers;

use App\ActivityType;
use Illuminate\Http\Request;

class ActivityTypesController extends Controller
{
    public function index()
    {
        $actype = new ActivityType();

        return $actype->all();
    }
}
