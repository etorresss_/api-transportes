<?php

namespace App\Http\Controllers;

use App\PetitionResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PetitionResourcesController extends Controller
{
    public function index($petitionId)
    {
        return PetitionResource::where('petition_id', $petitionId)->get();
    }

    public function store(Request $request) {
        return PetitionResource::create($request->all());
    }

    public function update(Request $request, $petitionId) {
        $resource = PetitionResource::findOrFail($petitionId);
        $resource->fill($request->all());

        $resource->save();

        return $resource;
    }

    public function destroy($petitionId)
    {
        PetitionResource::destroy($petitionId);

        return 1;
    }
}
