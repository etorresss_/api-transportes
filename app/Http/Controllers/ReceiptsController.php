<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Receipt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReceiptsController extends Controller
{
    public function index($petitionId) {
        return Petition::find($petitionId)->receipt;
    }

    public function show($id) {
        return Receipt::findOrFail($id);
    }

    public function store(Request $request) {
        return Receipt::create($request->all());
    }

    public function update(Request $request, $id) {
        $receipt = Receipt::findOrFail($id);
        $receipt->fill($request->all());

        $receipt->save();

        return $receipt;
    }

    public function averageCost() {
        $data = DB::table('receipts')
                    ->select(DB::raw('
                        AVG((final_km - starting_km) * 500 +
                        (TIMESTAMPDIFF(HOUR, start_time, end_time) * 1000)) as \'Costo Promedio\'
                    '))->get();
        return prepareReport($data);
    }
}
