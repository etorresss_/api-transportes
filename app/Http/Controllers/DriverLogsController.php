<?php

namespace App\Http\Controllers;

use App\DriverLog;
use App\Petition;
use Illuminate\Http\Request;

class DriverLogsController extends Controller
{
    public function index($petitionId) {
        return Petition::find($petitionId)->driverLogs;
    }

    public function show($id) {
        return DriverLog::findOrFail($id);
    }

    public function store(Request $request) {
        return DriverLog::create($request->all());
    }
}