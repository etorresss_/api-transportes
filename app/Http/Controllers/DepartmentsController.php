<?php

namespace App\Http\Controllers;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentsController extends Controller
{
    public function index() {
        $dep = new Department();
        
        return $dep->all();
    }

    public function show($id) {
        return Department::findOrFail($id);
    }

    public function store(Request $request) {
        return Department::create($request->all());
    }

    public function petitions($id) {
        return Department::find($id)->petitions;
    }

    public function byType($departmentId, $utId) {
        return User::where([
            ['department_id', $departmentId],
            ['user_type_id', $utId]
        ])->get();
    }

    public function mostPetitions(){
        $data = DB::table('departments')
                    ->join('petitions', 'departments.id', '=', 'petitions.department_id')
                    ->select('departments.name as Departamento', DB::raw('COUNT(petitions.department_id) as Cantidad'))
                    ->groupBy('departments.name')
                    ->orderBy('Cantidad', 'desc')
                    ->get();

        return prepareReport($data);
    }

    public function mostChanges(){
        $data = DB::table('departments')
                    ->join('petitions', 'departments.id', '=', 'petitions.department_id')
                    ->join('change_logs', 'petitions.id', '=', 'change_logs.petitions_id')
                    ->select('departments.name as Departamento', DB::raw('COUNT(*) as Cantidad'))
                    ->groupBy('departments.name')
                    ->orderBy('Cantidad', 'desc')
                    ->get();

        return prepareReport($data);
    }
}
