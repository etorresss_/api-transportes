<?php

namespace App\Http\Controllers;

use App\Petition;
use App\PetitionParticipant;
use Illuminate\Http\Request;

class PetitionParticipantsController extends Controller
{

    public function index($petitionId)
    {
        return Petition::find($petitionId)->petitionParticipants;
    }

    public function store(Request $request) {
        $participant = new PetitionParticipant($request->all());
        $participant->save();

        return $participant;
    }

    public function update(Request $request, $id) {
        $participant = PetitionParticipant::find($id);
        $participant->fill($request->all());
        $participant->save();

        return $participant;
    }

    public function destroy($id) {
        return PetitionParticipant::destroy($id);
    }
}
