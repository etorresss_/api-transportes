<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Petition extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'user_id', 'department_id', 'departure_time', 'arrival_time',
        'justification', 'activity_type_id', 'address', 'state_id',
        'needs_driver', 'functional_center_code', 'budget', 'petition_status_id',
        'needs_vice_rector'
    ];

    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function activityType() {
        return $this->belongsTo('App\ActivityType');
    }

    public function state() {
        return $this->belongsTo('App\State');
    }

    public function petitionParticipants() {
        return $this->hasMany('App\PetitionParticipant');
    }

    public function petitionStatus() {
        return $this->belongsTo('App\PetitionStatus');
    }

    public function petitionApprovals(){
        return $this->hasMany('App\PetitionApproval');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function driverLogs(){
        return $this->hasMany('App\DriverLog');
    }

    public function receipt() {
        return $this->hasOne('App\Receipt');
    }
}
