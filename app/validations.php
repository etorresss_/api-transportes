<?php

function prepareReport($results) {
    $data = [];
    $data['count'] = sizeof($results);
    $data['next'] = null;
    $data['previous'] = null;
    $data['results'] = $results;
    return $data;
}

function needsViceRector($departureTime, $arrivalTime) {
    $min = date('H:i:s', strtotime("07:30:00"));
    $max = date('H:i:s', strtotime("16:30:00"));
    $departure = date('H:i:s', strtotime($departureTime));
    $arrival = date('H:i:s', strtotime($arrivalTime));

    return $departure < $min || $arrival > $max;
}