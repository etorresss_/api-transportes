<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'name', 'lastname', 
        'email', 'phone', 'department_id', 'user_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function userType() {
        return $this->belongsTo('App\UserType');
    }

    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function petitionApprovals(){
        return $this->hasMany('App\PetitionApproval');
    }

    public function driverLogs(){
        return $this->hasMany('App\DriverLog');
    }
}
