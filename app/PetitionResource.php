<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetitionResource extends Model
{
    protected $fillable = [
        'petition_id', 'resource_type_id', 'user_id', 'value'
    ];

    public function petition() {
        return $this->belongsTo('App\Petition');
    }

    public function resourceType() {
        return $this->belongsTo('App\ResourceType');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
