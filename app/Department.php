<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function user() {
        return $this->hasMany('App\User');
    }

    public function petitions() {
        return $this->hasMany('App\Petition');
    }
}
