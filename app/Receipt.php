<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $fillable = [
        'petition_id', 'starting_km', 'final_km',
        'start_time', 'end_time'
    ];

    public function petition() {
        return $this->belongsTo('App\Petition');
    }
}
