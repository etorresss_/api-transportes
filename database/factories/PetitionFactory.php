<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Petition;
use App\User;
use Faker\Generator as Faker;

$factory->define(Petition::class, function (Faker $faker) {
    $id = $faker->randomElement([3, 8, 13, 18]);
    $user = User::find($id);
    $date = '2020-'.$faker->numberBetween(2, 12)
        .'-'.$faker->numberBetween(1, 28);
    $time1 = $faker->time();
    $time2 = $faker->time();
    $min = date('H:i:s', strtotime("07:30:00"));
    $max = date('H:i:s', strtotime("16:30:00"));
    
    if ($time1 > $time2) {
        $temp = $time1;
        $time1 = $time2;
        $time2 = $temp;
    }

    return [
        'user_id' => $user->id,
        'department_id' => $user->department_id, 
        'departure_time' => $date.' '.$time1, 
        'arrival_time' => $date.' '.$time2, 
        'justification' => $faker->realText(255, 2),
        'activity_type_id' => $faker->numberBetween(1, 3),
        'address' => $faker->address,
        'state_id' => $faker->numberBetween(1, 8),
        'functional_center_code' => $faker->randomNumber(3),
        'needs_driver' => $faker->boolean,
        'budget' => $faker->randomNumber(),
        'petition_status_id' => $faker->numberBetween(3, 5),
        'needs_vice_rector' => $time1 < $min || $time2 > $max
    ];
});
