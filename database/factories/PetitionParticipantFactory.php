<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\PetitionParticipant;
use Faker\Generator as Faker;

$factory->define(PetitionParticipant::class, function (Faker $faker) {
    return [
        'petition_id' => $faker->numberBetween(1, 10),
        'name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'identification_number' => $faker->randomNumber(),
        'school_id' => $faker->boolean ? $faker->randomNumber(6) : null,
        'pickup_address' => $faker->address,
        'dropoff_address' => $faker->address,
        'pickup_time' => $faker->time,
        'dropoff_time' => $faker->time, 
        'in_charge' => $faker->boolean
    ];
});
