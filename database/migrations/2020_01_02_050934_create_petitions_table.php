<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('department_id')->unsigned();
            $table->dateTime('departure_time');
            $table->dateTime('arrival_time');
            $table->text('justification');
            $table->bigInteger('activity_type_id')->unsigned();
            $table->string('address');
            $table->boolean('needs_driver');
            $table->bigInteger('state_id')->unsigned();
            $table->smallInteger('functional_center_code')->default(0);
            $table->integer('budget');
            $table->bigInteger('petition_status_id')->unsigned();
            $table->boolean('needs_vice_rector');
            $table->timestamps();
            $table->softDeletes();

            /* Foreign keys */
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('restrict');

            $table->foreign('department_id')
            ->references('id')->on('departments')
            ->onDelete('restrict');

            $table->foreign('activity_type_id')
            ->references('id')->on('activity_types')
            ->onDelete('restrict');

            $table->foreign('state_id')
            ->references('id')->on('states')
            ->onDelete('restrict');

            $table->foreign('petition_status_id')
            ->references('id')->on('petition_status')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petitions');
    }
}
