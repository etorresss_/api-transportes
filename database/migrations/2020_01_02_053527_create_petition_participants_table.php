<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetitionParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petition_participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('petition_id')->unsigned();
            $table->string('name');
            $table->string('last_name');
            $table->bigInteger('identification_number')->unsigned();
            $table->integer('school_id')->nullable();
            $table->string('pickup_address')->nullable();
            $table->string('dropoff_address')->nullable();
            $table->time('pickup_time')->nullable();
            $table->time('dropoff_time')->nullable();
            $table->boolean('in_charge');
            $table->timestamps();

            /* Foreign keys */
            $table->foreign('petition_id')
            ->references('id')->on('petitions')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petition_participants');
    }
}
