<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('petition_id')->unsigned();
            $table->string('destination');
            $table->bigInteger('checked_by')->unsigned();
            $table->boolean('approved');
            $table->string('comment')->nullable();
            $table->timestamps();

            /* Foreign keys */
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('restrict');

            $table->foreign('petition_id')
            ->references('id')->on('petitions')
            ->onDelete('cascade');

            $table->foreign('checked_by')
            ->references('id')->on('petition_participants')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_logs');
    }
}
