<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('petition_id')->unsigned();
            $table->integer('starting_km');
            $table->integer('final_km');
            $table->time('start_time');
            $table->time('end_time');
            $table->timestamps();

            /* Foreign keys */
            $table->foreign('petition_id')
            ->references('id')->on('petitions')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
