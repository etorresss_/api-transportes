<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetitionResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petition_resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('petition_id')->unsigned();
            $table->bigInteger('resource_type_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('value');
            $table->timestamps();

            /* Foreign keys */
            $table->foreign('petition_id')
            ->references('id')->on('petitions')
            ->onDelete('cascade');

            $table->foreign('resource_type_id')
            ->references('id')->on('resource_types')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petition_resources');
    }
}
