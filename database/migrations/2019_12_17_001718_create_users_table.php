<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('lastname');
            $table->string('email');
            $table->string('phone');
            $table->bigInteger('department_id')->unsigned();
            $table->bigInteger('user_type_id')->unsigned();
            $table->timestamps();

            /* Foreign keys */
            $table->foreign('user_type_id')
            ->references('id')->on('user_types')
            ->onDelete('restrict');
            
            $table->foreign('department_id')
            ->references('id')->on('departments')
            ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
