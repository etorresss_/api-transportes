<?php

use Illuminate\Database\Seeder;

class ResourceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resource_types')->insert([
            ['name' => 'Conductor', 'created_at' => now()],
            ['name' => 'Vehículo', 'created_at' => now()],
        ]);
    }
}
