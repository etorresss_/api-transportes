<?php

use App\DriverLog;
use App\Petition;
use App\PetitionParticipant;
use App\PetitionResource;
use Illuminate\Database\Seeder;

class DriverLogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $petitions = Petition::where([
            ['petition_status_id', 5],
            ['needs_driver', true]])->get();

        foreach ($petitions as $p) {
            $driver = PetitionResource::where([
                ['petition_id', $p->id],
                ['resource_type_id', 1]
            ])->get()[0];

            $participant = PetitionParticipant::where([
                ['petition_id', $p->id],
                ['in_charge', true]
            ])->get()[0];

            DriverLog::create([
                'user_id' => $driver->id,
                'petition_id' => $p->id,
                'destination' => $p->address,
                'checked_by' => $participant->id,
                'approved' => true
            ]);

            DriverLog::create([
                'user_id' => $driver->id,
                'petition_id' => $p->id,
                'destination' => 'Tecnológico de Costa Rica - Sede Cartago',
                'checked_by' => $participant->id,
                'approved' => true
            ]);
        }
    }
}
