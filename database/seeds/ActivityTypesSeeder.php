<?php

use Illuminate\Database\Seeder;

class ActivityTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_types')->insert([
            ['name' => 'Administrativa', 'created_at' => now()],
            ['name' => 'Docente', 'created_at' => now()],
            ['name' => 'Investigación', 'created_at' => now()],
        ]);
    }
}
