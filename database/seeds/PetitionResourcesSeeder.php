<?php

use App\Petition;
use App\PetitionResource;
use App\User;
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class PetitionResourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $petitions = Petition::where('petition_status_id', 5)->get();

        foreach ($petitions as $p) {
            $employee = DB::table('users')
                            ->where('user_type_id', 4)
                            ->get()[0];
            $resource = new PetitionResource([
                'petition_id' => $p->id,
                'resource_type_id' => 2,
                'user_id' => $employee->id,
                'value' => $faker->regexify('[A-Z-[AEIOU]]{3}-[0-9]{3}')
            ]);

            $resource->save();

            if ($p->needs_driver) {
                $drivers = User::where('user_type_id', 5)->get();

                $driverId = $drivers[$faker->numberBetween(0, sizeof($drivers) - 1)]->id;
                $resource = new PetitionResource([
                    'petition_id' => $p->id,
                    'resource_type_id' => 1,
                    'user_id' => $employee->id,
                    'value' => $driverId
                ]);
            }

            $resource->save();
        }
    }
}
