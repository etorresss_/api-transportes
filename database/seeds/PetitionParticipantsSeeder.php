<?php

use App\PetitionParticipant;
use Illuminate\Database\Seeder;

class PetitionParticipantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PetitionParticipant::class, 100)->create();
    }
}
