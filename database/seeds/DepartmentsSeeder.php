<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            ['name' => 'Rectoría'],
            ['name' => 'Transportes'],
            ['name' => 'Escuela de Ingeniería en Computación'],
            ['name' => 'Centro de Investigación de Computación']
        ]);
    }
}
