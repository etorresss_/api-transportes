<?php

use Illuminate\Database\Seeder;

class PetitionStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('petition_status')->insert([
            ['name' => 'Pendiente - Jefe de departamento', 'created_at' => now()],
            ['name' => 'Pendiente - Rectoría', 'created_at' => now()],
            ['name' => 'Pendiente - Departamento de transportes', 'created_at' => now()],
            ['name' => 'Rechazada', 'created_at' => now()],
            ['name' => 'Aprobada', 'created_at' => now()],
        ]);
    }
}
