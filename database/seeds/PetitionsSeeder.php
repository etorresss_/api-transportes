<?php

use App\Petition;
use Illuminate\Database\Seeder;

class PetitionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Petition::class, 10)->create();
    }
}
