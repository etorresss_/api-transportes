<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypesSeeder::class);
        $this->call(ActivityTypesSeeder::class);
        $this->call(ResourceTypesSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(DepartmentsSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(PetitionStatusSeeder::class);
        $this->call(PetitionsSeeder::class);
        $this->call(PetitionParticipantsSeeder::class);
        $this->call(ReceiptsSeeder::class);
        $this->call(PetitionResourcesSeeder::class);
        $this->call(DriverLogsSeeder::class);
    }
}
