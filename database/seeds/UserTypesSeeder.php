<?php

use Illuminate\Database\Seeder;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = now();
        DB::table('user_types')->insert([
            ['name' => 'Rectoría', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Jefe de departamento', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Funcionario', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Jefe de transportes', 'created_at' => $date, 'updated_at' => $date],
            ['name' => 'Chofer', 'created_at' => $date, 'updated_at' => $date],
        ]);
    }
}
