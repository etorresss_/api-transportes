<?php

use App\Petition;
use App\Receipt;
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class ReceiptsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $petitions = Petition::where('petition_status_id', 3)->get();

        foreach ($petitions as $p) {
            $km = $faker->randomNumber();

            Receipt::create([
                'petition_id' => $p->id,
                'starting_km' => $km,
                'final_km' => $km + $faker->numberBetween(30, 400),
                'start_time' => $p->departure_time,
                'end_time' => $p->arrival_time
            ]);
        }
    }
}
