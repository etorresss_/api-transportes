<?php

use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            ['name' => 'San José', 'created_at' => now()],
            ['name' => 'Alajuela', 'created_at' => now()],
            ['name' => 'Cartago', 'created_at' => now()],
            ['name' => 'Heredia', 'created_at' => now()],
            ['name' => 'Guanacaste', 'created_at' => now()],
            ['name' => 'Puntarenas', 'created_at' => now()],
            ['name' => 'Limón', 'created_at' => now()],
            ['name' => 'Otro', 'created_at' => now()],
        ]);
    }
}
