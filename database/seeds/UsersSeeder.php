<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        foreach(range(1, 20) as $i) {
            $type = $i%5 === 0 ? 5 : $i%5;
            $dep = 1;
            // usuario tipo personal de transportes o chofer
            if ($type > 3) {
                $dep = 2;
            // usuarios de IC
            } else if ($type > 1) {
                $dep = $faker->numberBetween(3, 4);
            }
            // Si no se asigna es usuario de rectoria
            
            DB::table('users')->insert([
                'username' => $faker->unique()->userName,
                'password' => Hash::make('1234'),
                'name' => $faker->firstName,
                'lastname' => $faker->lastname,
                'email' => $faker->safeEmail,
                'phone' => $faker->phoneNumber,
                'user_type_id' => $type,
                'department_id' => $dep,
                'created_at' => now()
            ]);
        }
    }
}
