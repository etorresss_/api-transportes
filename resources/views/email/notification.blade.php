<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $subject }}</title>
</head>
<body>
    <h1>{{ $title }}</h1>
    <ul>
        <li>Departamento: {{ $department->name }}</li>
        <li>Emisor: {{ $user->name.' '.$user->lastname }}</li>
        <li>Dirección: {{ $petition->address }}</li>
        <li>Fecha de emisión: {{ $petition->created_at }}</li>
    </ul>

    <p>Ve los detalles completos en la aplicación móvil.</p>
</body>
</html>